export interface Work{
    id:number,
    title:string,
    writer: number
}

export const works:Work[] = [
    {"id": 9780753519509, "title": "Groucho and Me", "writer": 3},
    {"id": 9781596987999, "title": "Das Kapital", "writer": 1},
    {"id": 9789061430018, "title": "Manifest der Kommunistischen Partei", "writer": 1}
];

export interface Animal{
    ownerId: number,
    id:number,
    name:string,
    species: string
}

export const animals:Animal[] = [
    {ownerId: 1,id: 1, name: "Choufke", species: "chicken", },
    {ownerId: 3,id: 1, name: "Figaro", species: "cat"},
    {ownerId: 3,id: 2, name: "Ram6", species: "dog"}
];

import express, {Request, Response} from "express";
import {router as jsonRouter} from "./routes/jsonRouter.js";

const app = express();
app.listen(3000);

app.use("/persons", jsonRouter);
app.use(express.static("./public"));

app.get("/about", (req: Request, res: Response) => {
	res.send("data server demo")
});

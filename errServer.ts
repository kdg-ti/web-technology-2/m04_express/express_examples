import express, {Request, Response} from "express";
import {persons} from "./repositories/personRepository.js"
import {HttpError} from "./model/middleware.js";

const app= express();
app.listen(3000);

const router = express.Router();
app.use("/persons",router);

router.get("/",(req:Request, res:Response) => {res.json(persons)});
router.get("/:persId", getPersById);



function getPersById(req:Request, resp:Response, next:any) {
	const person = persons.find(p => p.id === +req.params.persId)
	if (person) {resp.json(person);}
	else {
		const r:HttpError = new Error(`Missing person: ${req.params.persId}!`);
		r.status =404;
		next(r);
	}
}

app.use((error:Error, req:Request, resp:Response, next:any) => {
		console.error(error.stack);
		if("status" in error){
		resp.status((error as HttpError).status!);}
		//resp.status(error.status!).send(error.message);
		resp.send(error.message);
	}
)

app.get("/about", ( req:Request, res:Response) => {res.json({name: "jsonServer", version: "0.3.0"})});

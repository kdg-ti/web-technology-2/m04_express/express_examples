import express, {Request, Response} from "express"
import logger  from "morgan"
import {router as restRouter} from "./routes/restRouter.js"

const app= express();
app.listen(3000);

app.use(logger("dev"))
app.use("/api/persons",restRouter);
app.use(express.static("./public"));


app.get("/about",(req:Request,res:Response) => {res.send("REST server demo")});



import express, {Request, Response} from "express";
import {persons} from "./repositories/personRepository.js"

const app= express();
app.listen(3000);

const router = express.Router();
app.use("/persons",router);

router.get("/",(req:Request, res:Response) => {res.json(persons)});
router.get("/:persId", (req:Request, res:Response) => {res.json(persons.find(p => p.id === +req.params.persId))});
app.get("/about", (req:Request, res:Response) => {res.json({name: "jsonServer", version: "0.3.0"})});

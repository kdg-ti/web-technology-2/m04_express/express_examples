import express, { Request, Response} from "express";
const app= express();

const PORT=3000;
app.listen(PORT);

app.use(express.static("./public"));


app.get("/hello",(req:Request,res:Response) => {res.send("No hello, goodbye!")});
console.log(`Server started on http://localhost:${PORT}`);



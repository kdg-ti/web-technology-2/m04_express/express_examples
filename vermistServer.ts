import express, {Request, Response} from "express";
import {persons} from "./repositories/personRepository.js"

const app= express();
app.listen(3000);

const router = express.Router();
app.use("/persons",router);

router.get("/",(req:Request, res:Response)  => {res.json(persons)});
router.get("/:persId",  getPersById);

function getPersById(req:Request, resp:Response) {
	const person = persons.find(p => p.id === +req.params.persId)
	if (person){
		resp.json(person);
	} else{ // zend foutboodschap als person niet gevonden
		resp.status(404).send(`Missing: ${req.params.persId}!`);
	}
}



app.get("/about", (req, res) => {res.json({name: "jsonServer", version: "0.3.0"})});


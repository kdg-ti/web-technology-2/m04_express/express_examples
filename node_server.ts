import  {createServer, IncomingMessage, ServerResponse} from "node:http";

const port = 3000;

function handleRequest(request:IncomingMessage, response:ServerResponse) {
	console.log(`URL requested: ${request.url}`)
	response.writeHead(200, {'Content-Type': 'text/plain'});
	response.end("I'm a node.js server!");
}

let srv= createServer(handleRequest);
srv.listen(port);
console.log(`Server started on http://localhost:${port}!'`)

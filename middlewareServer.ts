import express, {Request, Response} from "express";

const app = express();
app.listen(3000);

app.use(middleMan);
// Log all routes
function middleMan(req:Request, res:Response, next:any) {
  console.log(`URL requested: ${req.url}`);
  next();
}

app.get("/", (req:Request, res:Response) => {res.send("Hello world")});



import {Request} from "express";
import {Person} from "../repositories/personRepository.js"
export interface HttpError  extends Error{
    status?: number;
}

export interface PersonRequest extends Request{
    person: Person;
}

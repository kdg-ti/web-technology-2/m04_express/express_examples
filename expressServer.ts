import express, { Request, Response} from "express";
const port = 3000;
const srv= express();
srv.listen(port);
console.log(`Server started on http://localhost:${port}!'`)

srv.get("/",handleGetRoot);
function handleGetRoot(request:Request, response:Response) {
	console.log(`URL requested: ${request.url}`)
	response.send("I'm an express server!");
}

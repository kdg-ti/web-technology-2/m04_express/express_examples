import express, {Request, Response} from "express";
import {persons} from "./repositories/personRepository.js"

const app = express();
const PORT = 3000;
app.listen(PORT);


app.get("/persons", (req: Request, res: Response) => {
	res.json(persons)
});
//app.get("/persons/1", (req:Request, res:Response) => res.json(persons.find(p => p.id === 1)));
app.get("/persons/:persId", (req: Request, res: Response) => {
	res.json(persons.find(p => p.id === +req.params.persId))
});
// if you want to use this change /persons to /persons/all otherwise /persons will handle the request
// app.get("/persons/{:persId}", (req: Request, res: Response) => {
// 	res.json(persons.find(p => p.id === +req.params.persId))
// });
app.get("/about", (req: Request, res: Response) => {
	res.json({name: "jsonServer", version: "0.3.0"})
});
console.log(`Server started on http://localhost:${PORT}`);



import express, {Request, Response} from "express";
import {persons} from "../repositories/personRepository.js"

export const router = express.Router();
router.get("/", (req: Request, res: Response) => {
	res.json(persons);
});

router.get("/:persId", (req: Request, res: Response) => {
	res.json(persons.find(p => p.id === +req.params.persId));
});

import express, {Request, Response} from "express";
import {persons} from "../repositories/personRepository.js"
import {animals} from "../repositories/animalRepository.js"
import {router as worksRouter} from "./subRouter.js"

export const router = express.Router();

router.use(express.json())
router.use("/:id/works", worksRouter);


let nextKey = Math.max.apply(Math, persons.map(p => p.id)) + 1


router.get("/", getPersons);
router.get("/:id", (req: Request, res: Response) => {
	res.json(persons.find(p => p.id === +req.params.id))
});
router.put("/:id", changePerson);

router.delete("/:id", deletePerson);
router.post("/", addPerson);
router.get("/:ownerId/animals/:animalId", (req: Request, res: Response) => {
	res.json(animals.find(a => a.ownerId === +req.params.ownerId && a.id === +req.params.animalId))
});

function changePerson(req: Request, res: Response) {
	const idx = persons.findIndex(p => p.id === +req.params.id);
	req.body.id = +req.body.id;
	if (idx >= 0) {
		persons[idx] = req.body;
		res.status(200);
	} else {
		persons.push(req.body)
		res.status(201);
		if (req.body.id >= nextKey) {
			nextKey = req.body.id + 1;
		}
	}
	res.send();
}

function addPerson(req: Request, res: Response) {
	req.body.id = nextKey
	persons.push(req.body);
	res.status(201)
		.location(`${req.originalUrl}/${nextKey}`)
		.send();
	nextKey++;
}

function getPersons(req: Request, res: Response) {
	const name = req.query.name
	const found = name
		? persons.filter(p => p.name == name)  // if name present filter
		: persons // else no query: return all
	if (found.length > 0) res.json(found)
	else res.sendStatus(404)
}

function deletePerson(req: Request, res: Response) {
	const idx = persons.findIndex(p => p.id === +req.params.id);
	if (idx >= 0) {
		persons.splice(idx, 1)
		res.sendStatus(204);
	} else {
		res.sendStatus(404)
	}
}

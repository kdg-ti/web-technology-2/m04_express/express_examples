import {works} from "../repositories/workRepository.js"

import express, {Request, Response} from "express"
export const router = express.Router({mergeParams: true});

router.get("/", getWorks);

function getWorks(req:Request,res:Response) {
	const result = works.filter(w => w.writer === +req.params.id)
	if (result.length !== 0) res.json(result)
	else res.sendStatus(404)
}

router.get("/:work", getWork);
router.delete("/:work", deleteWork);

function getWork(req:Request,res:Response) {
	const result = works.filter(w => w.writer ===  parseInt(req.params.id));
	if (!result || result.length === 0) {
		res.sendStatus(404);
		return;
	}

	let work = result.find(({id}) => id ===  parseInt(req.params.werk));
	if (!work ) {
		res.sendStatus(404);
		return;
	}

	res.json(work)
}

function deleteWork(req:Request,res:Response){
	const result = works.filter(w => w.writer ===  parseInt(req.params.id));
	if (result.length === 0) {
		res.sendStatus(404);
		return;
	}

	const idx = works.findIndex(({id}) => id === parseInt(req.params.werk));

	if (idx >= 0) {
		works.splice(idx, 1)
		res.sendStatus(204);
	} else {
		res.sendStatus(404)
	}
}
